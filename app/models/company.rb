class Company < ActiveRecord::Base
  has_many :vacancies
  has_many :users, :through => :users_vacancies
end
