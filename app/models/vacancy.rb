class Vacancy < ActiveRecord::Base
  belongs_to :company
  # has_and_belongs_to_many :skills
  has_and_belongs_to_many :users
end
