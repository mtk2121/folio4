json.array!(@vacancies) do |vacancy|
  json.extract! vacancy, :company_id, :position, :description, :location, :from_home, :salary, :show_salary, :interview_address, :ongoing
  json.url vacancy_url(vacancy, format: :json)
end
