json.array!(@companies) do |company|
  json.extract! company, :name, :status, :tax_number, :email, :password, :contact_person_first_name, :contact_person_last_name, :contact_person_mobile_number, :contact_person_email, :website, :office_phone_number, :description, :mail_verified, :company_verified, :address, :logo_large, :logo_small, :permalink
  json.url company_url(company, format: :json)
end
