class VacanciesController < ApplicationController
  before_action :set_vacancy, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:index, :show]

  def index
    @vacancies = Vacancy.all
  end

  def show
  end

  def new
    @vacancy = current_user.vacancies.build
  end

  def edit
  end

  def create
    @vacancy = current_user.vacancies.build(vacancy_params)

    if @vacancy.save
      redirect_to @vacancy, notice: 'Vacancy was successfully created.' 
    else
      render action: 'new'
     end
  end

  
  def update
    if @vacancy.update(vacancy_params)
      redirect_to @vacancy, notice: 'Vacancy was successfully updated.'
    else
      render action: 'edit'
    end
  end

  def destroy
    @vacancy.destroy
    redirect_to vacancies_url
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vacancy
      @vacancy = Vacancy.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def vacancy_params
      params.require(:vacancy).permit(:company_id, :position, :description, :location, :from_home, :salary, :show_salary, :interview_address, :ongiong)
    end
end
