class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:index, :show]
  
  def index
  end

  def edit
  end

  def new
     @user = User.create( params[:user] )
  end
  
  def create
     @user = User.create( params[:user] )
  end

  def show
  end
end
