class CreateJoinTable < ActiveRecord::Migration
  def change
    create_join_table :users, :vacancies do |t|
      # t.index [:user_id, :vacancy_id]
      # t.index [:vacancy_id, :user_id]
    end
  end
end
