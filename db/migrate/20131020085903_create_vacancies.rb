class CreateVacancies < ActiveRecord::Migration
  def change
    create_table :vacancies do |t|
      t.integer :company_id
      t.string :position
      t.text :description
      t.string :location
      t.boolean :from_home
      t.integer :salary
      t.boolean :show_salary
      t.string :interview_address
      t.boolean :ongoing

      t.timestamps
    end
  end
end
