class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.string :status
      t.integer :tax_number
      t.string :email
      t.string :password
      t.string :contact_person_first_name
      t.string :contact_person_last_name
      t.string :contact_person_mobile_number
      t.string :contact_person_email
      t.string :website
      t.string :office_phone_number
      t.text :description
      t.boolean :mail_verified
      t.boolean :company_verified
      t.string :address
      t.string :logo_large
      t.string :logo_small
      t.string :permalink

      t.timestamps
    end
    add_index :companies, :permalink
  end
end
