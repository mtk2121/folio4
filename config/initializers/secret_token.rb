# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Folio4::Application.config.secret_key_base = '7f5bc63e35f86c7d7ac1f4e55653eccc409a30e5545812a781df715c2f6adc00cff2c12027976bb2fe197b17134a4b21f1aa041a47532e2b3cc11fdfd0ef27ba'
